package com.chr.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.chr.domain.Student;
import com.chr.domain.User;
import com.chr.service.StudentService;
import com.chr.service.impl.UserOperationsServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:config/spring-context.xml","classpath:config/redis-context.xml" })
public class RedisTest {

	@Autowired
	private UserOperationsServiceImpl userops;
	
	@Resource
	StudentService studentService;

	//@Test
	public void Test1() {
		User user = new User("1", "chenhaoran", "admin");
		userops.add(user);
		User user1 = userops.getUser(user.getId());
		System.out.println(user1);
		System.out.println(user1.getId() + user1.getName()
				+ user1.getPassword());
	}
	
	//@Test
	public void testInsert(){
		Student stu = new Student();
		stu.setId("5");
		stu.setName("aaaaaassssss");
		stu.setAge(11);
		studentService.insert(stu);
	}
	
	@Test
	public void testQueryById(){
		Student stu = 	studentService.queryById("3");
		if(stu!=null){
			System.out.println(stu.getName());
			System.out.println("66666666666666666666");
		}else{
			System.out.println("7777777777777777777777");
		}
	
		
	}
	
	//@Test
	public void testDelete(){
		studentService.delete("5");
	}
	
	//@Test
	public void testUpdate(){
		Student stu = new Student();
		stu.setId("5");
		stu.setName("ggggggggggggggg");
		stu.setAge(11);
		studentService.update(stu);
	}
	
	
	
}
