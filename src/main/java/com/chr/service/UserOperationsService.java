package com.chr.service;

import org.springframework.stereotype.Component;

import com.chr.domain.User;

/**
 * @author Edwin Chen
 *
 */
@Component
public interface UserOperationsService {
	void add(User user);
	User getUser(String key);
	
}
