package com.chr.service.impl;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.chr.dao.StudentMapper;
import com.chr.domain.Student;
import com.chr.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Resource
	StudentMapper studentMapper;
	@CachePut(key="#stu.id",value="stu")
	@Override
	public Student insert(Student stu) {
		studentMapper.insert(stu);
		return stu;
	}

	@CacheEvict(key="#id",value="stu")
	@Override
	public void delete(String id) {
		studentMapper.delete(id);
	}

	@CachePut(key="#stu.id",value="stu")
	@Override
	public Student update(Student stu) {
		System.out.println("aaaaaaaaaaaaa");
		studentMapper.update(stu);
		System.out.println("bbbbbbbbbbbb");
		return stu;
	}

	@Cacheable(key="#id",value="StudentServiceImpl.queryById")
	@Override
	public Student queryById(String id) {
		System.out.println("111111111111111111");
		Student stu =  studentMapper.queryById(id);
		System.out.println("222222222222222222");
		return stu;
	}

}
