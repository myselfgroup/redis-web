package com.chr.service;

import com.chr.domain.Student;

public interface StudentService {

	public Student insert(Student stu);
	public void delete(String id);
	public Student update(Student stu);
	public Student queryById(String id);
}
