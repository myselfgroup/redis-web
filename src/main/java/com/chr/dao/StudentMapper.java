package com.chr.dao;

import org.springframework.stereotype.Repository;

import com.chr.domain.Student;

@Repository
public interface StudentMapper {
	
	public void insert(Student stu);
	public void delete(String id);
	public void update(Student stu);
	public Student queryById(String id);
	
}
