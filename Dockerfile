FROM tomcat

MAINTAINER zhoujinpen <550874584@qq.com>

ENV WEBAPPS_HOME=$CATALINA_HOME/webapps

ADD target/redis-web-0.0.1-SNAPSHOT.war $WEBAPPS_HOME/

EXPOSE 8080
CMD ["catalina.sh", "run"]
