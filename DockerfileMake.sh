#!/bin/bash	 
version=0.0.1
#stop rm container
id=`sudo docker ps -a |grep 'redis-web':$1 |awk '{print $1}'`
if [ -n "$id" ];
then
docker stop $id
docker rm $id
else
echo "no running docker container"
fi
# rmi images
imgid=`sudo docker images |grep 'redis-web'| awk '{print $3}'`
if [ -n "$imgid" ];
then
docker rmi $imgid
else
echo "no docker image"
fi
#sudo build image
sudo docker build -t zhoujinpen/redis-web:$version . \
	 --label vendor=XINYAN \
	 --label name=redis-web
#sudo docker push zhoujinpen/redis-web:0.0.1
#run image
sudo docker run -itd -p 8081:8080 --name redis-web zhoujinpen/redis-web:$version

#sudo docker rmi zhoujinpen/redis-web:0.0.1
